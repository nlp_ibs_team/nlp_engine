from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
from textblob import TextBlob


def polariser(score_look):
    pol = ((score_look - (-1)) / (1 - (-1)))
    return pol


@csrf_exempt
def Sentiment_Analysis(request, text_body=None):
    if request.method == 'POST':
        req_data = {}
        analysis_data = {}
        if request.POST:
            text = request.POST.get('text_body', None)
        else:
            text = text_body
        Sentence4Analysis = text
        input_text = TextBlob(Sentence4Analysis)
        Length_Of_Sentence = len(input_text.sentences)
        array_of_sentences = []
        array_of_score = []
        if (Length_Of_Sentence < 3):
            input_sentence = str(input_text)
            array_of_sentences.append(input_sentence)
            score_look = input_text.sentiment.polarity
            score_look = polariser(score_look)
            array_of_score.append(score_look)
        else:
            for sentence in input_text.sentences:
                input_sentence = str(sentence)
                array_of_sentences.append(input_sentence)
                score_look = sentence.sentiment.polarity
                score_look = polariser(score_look)
                array_of_score.append(score_look)
        len_of_array = len(array_of_sentences)
        print(len_of_array)
        array_of_sentence_graph = []
        for i in range(1, len_of_array + 1):
            array_of_sentence_graph.append("Sentence " + str(i))
        graphdatasource_list = []
        for key, val in zip(array_of_sentence_graph, array_of_score):
            datasource = {}
            datasource['sentence'] = key
            datasource['value'] = val
            graphdatasource_list.append(datasource)
        if text_body:
                return graphdatasource_list
        else:
                return JsonResponse(graphdatasource_list, safe=False)
