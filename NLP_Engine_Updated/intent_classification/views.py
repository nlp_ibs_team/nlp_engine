from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
from intent_classification.intent_classifer import choose_all_models_mail_bot,choose_all_models_chat_bot, run_prediction_tests
from operator import itemgetter
import pickle

@csrf_exempt
def Intent_Classification_Mail_Train(request):
    choose_all_models_mail_bot()
    return JsonResponse("Success", safe=False)

@csrf_exempt
def Intent_Classification_Chat_Train(request):
    choose_all_models_chat_bot()
    return JsonResponse("Success", safe=False)

@csrf_exempt
def Intent_Classification_Mail(request,text_body=None):
    print("*************Mail********************")
    parse_response = {}
    text = ''
    if request.POST:
        text = request.POST.get('text_body', None)

    else:
        text=text_body
    model_list =[]
    with open('home/ubuntu/nlp_engine/NLP_Engine_Updated/intent_classification/models/indent_classifiers.models' , 'rb') as pickle_file:
        models_list = pickle.load(pickle_file)
    predicted_value_keys = dict()
    for i in range(5):
        predicted_value = models_list[i]["model"].predict(models_list[i]["vectorizer"].transform([text]))
        if predicted_value[0] in predicted_value_keys:
            predicted_value_keys[predicted_value[0]] += 0.20
        else:
            predicted_value_keys[predicted_value[0]] = 0.20
    actual_predicted_value = sorted(predicted_value_keys.items(), key=itemgetter(1), reverse=True)
    parse_response['Response'] = actual_predicted_value[0]
    if text_body:
        return parse_response
    else:
        return JsonResponse(parse_response, safe=False)


@csrf_exempt
def Intent_Classification_Chat(request,text_body=None):
    print("*************Chat********************")
    parse_response = {}
    text = ''
    if request.POST:
        text = request.POST.get('text_body', None)
    else:
        text=text_body
    models_list = []
    with open('home/ubuntu/nlp_engine/NLP_Engine_Updated/intent_classification/models/indent_classifiers_chat_bot.models', 'rb') as pickle_file:
        models_list = pickle.load(pickle_file)
    predicted_value_keys = dict()
    for i in range(5):
        predicted_value = models_list[i]["model"].predict(models_list[i]["vectorizer"].transform([text]))
        if predicted_value[0] in predicted_value_keys:
            predicted_value_keys[predicted_value[0]] += 0.20
        else:
            predicted_value_keys[predicted_value[0]] = 0.20

    actual_predicted_value = sorted(predicted_value_keys.items(), key=itemgetter(1), reverse=True)
    parse_response['Response'] = actual_predicted_value[0]
    if text_body:
        return parse_response
    else:
        return JsonResponse(parse_response, safe=False)
