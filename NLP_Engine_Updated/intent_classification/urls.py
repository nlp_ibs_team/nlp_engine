from django.conf.urls import url
from .views import  Intent_Classification_Mail,Intent_Classification_Mail_Train,Intent_Classification_Chat,Intent_Classification_Chat_Train
urlpatterns = [

    url(r'^intent_classification_mail/', Intent_Classification_Mail, name="intent_classification_mail"),
    url(r'^intent_mail_train/', Intent_Classification_Mail_Train, name="intent_mail_train"),
    url(r'^intent_classification_chat/', Intent_Classification_Chat, name="intent_classification_chat"),
    url(r'^intent_chat_train/', Intent_Classification_Chat_Train, name="intent_chat_train"),
]