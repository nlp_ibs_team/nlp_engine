from django.shortcuts import render
import json
from django.http import HttpResponse
import spacy
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from entity_recognition.views import entity_recognition,entity_recognition_cargo
from intent_classification.views import Intent_Classification_Mail,Intent_Classification_Chat
from sentiment_analysis.views import Sentiment_Analysis
import requests
import re

import asyncio


parse_response = {}

tasks = ['er','sa','ir']
async def get_nlp_task(nlp_task,request, nlp_text,app_id):
    print("app_id",type(app_id))
    if nlp_task == 'er':
        if app_id == '2':
            er_res = entity_recognition_cargo(request, nlp_text)
        else:
            er_res = entity_recognition(request, nlp_text)
        if er_res:
             parse_response['Entity_recognition'] = er_res
    elif nlp_task == 'sa':
        sa_res = Sentiment_Analysis(request, nlp_text)
        if sa_res:
            parse_response['Sentiment_Analysis'] = sa_res
    else:
        if nlp_text:

            nlp = spacy.load('en_core_web_sm')
            doc = nlp(nlp_text)
            array_of_sentence = []
            for token in doc.sents:
                array_of_sentence.append(token)
            input_length = len(array_of_sentence)
            if (input_length < 3):
                intent_classi_chat_res = Intent_Classification_Chat(request, nlp_text)
                if intent_classi_chat_res:
                    parse_response['Intent_Classification'] = intent_classi_chat_res
            else:
                intent_classi_mail_res = Intent_Classification_Mail(request, nlp_text)
                if intent_classi_mail_res:
                    parse_response['Intent_Classification'] = intent_classi_mail_res
    return parse_response




def clean_sentence(text):
   new_string = ''
   if text:
        new_string = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",text).split())
   return new_string

async def call_nlp_task(task,request, nlp_text,app_id):

    response = await get_nlp_task(task,request, nlp_text,app_id)

    return response



@csrf_exempt
def CommonAPI(request):

    nlp_text = ''
    if request.POST:
        res_data = ''
        parse_response.clear()
        res_list = []
        response_data = ''
        nlp_text = request.POST.get('text_body', None)
        app_id = request.POST.get('app_id', None)
        print("app_id from req",app_id)
        if nlp_text:
            nlp_text = clean_sentence(nlp_text)
            futures = [call_nlp_task(task,request, nlp_text,app_id) for task in tasks]
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            res_datas, b = loop.run_until_complete(asyncio.wait(futures))
            if res_datas:
                try:
                   for res_data in res_datas:
                        res_list.append(res_data.result())
                except:
                    print("Error in Result")
            loop.close()
        if res_list:
            response_data = res_list[-1]
            print(response_data)
        return JsonResponse(response_data, safe=False)






