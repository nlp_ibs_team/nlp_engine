from django.shortcuts import render
import json
from django.http import HttpResponse
import spacy
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from entity_recognition.views import entity_recognition
from intent_classification.views import Intent_Classification_Mail,Intent_Classification_Chat
from sentiment_analysis.views import Sentiment_Analysis
import requests
import asyncio


parse_response = {}

tasks = ['er','sa','ir']
async def get_nlp_task(nlp_task,request, nlp_text):
    if nlp_task == 'er':
        er_res = entity_recognition(request, nlp_text)
        if er_res:
             parse_response['Entity recognition'] = er_res
    elif nlp_task == 'sa':
        sa_res = Sentiment_Analysis(request, nlp_text)
        if sa_res:
            parse_response['Sentiment Analysis'] = sa_res
    else:
        if nlp_text:
            nlp = spacy.load('en_core_web_sm')
            doc = nlp(nlp_text)
            array_of_sentence = []
            for token in doc.sents:
                array_of_sentence.append(token)
            input_length = len(array_of_sentence)
            if (input_length < 3):
                intent_classi_chat_res = Intent_Classification_Chat(request, nlp_text)
                if intent_classi_chat_res:
                    parse_response['Intent Classification'] = intent_classi_chat_res
            else:
                intent_classi_mail_res = Intent_Classification_Mail(request, nlp_text)
                if intent_classi_mail_res:
                    parse_response['Intent Classification'] = intent_classi_mail_res
    return parse_response


async def call_nlp_task(task,request, nlp_text):

    response = await get_nlp_task(task,request, nlp_text)

    return response



@csrf_exempt
def CommonAPIChat(request):

    nlp_text = ''
    parse_response.clear()
    if request.POST:
        nlp_text = request.POST.get('text_body', None)
        if nlp_text:
            futures = [call_nlp_task(task,request, nlp_text) for task in tasks]
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            a, b = loop.run_until_complete(asyncio.wait(futures))
            loop.close()
        return JsonResponse(parse_response, safe=False)


@csrf_exempt
def CommonAPIChat(request):

    nlp_text = ''
    parse_response.clear()
    if request.POST:
        nlp_text = request.POST.get('text_body', None)
        if nlp_text:
            futures = [call_nlp_task(task,request, nlp_text) for task in tasks]
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            a, b = loop.run_until_complete(asyncio.wait(futures))
            loop.close()
        return JsonResponse(parse_response, safe=False)


@csrf_exempt
def CommonAPIMail(request):

    nlp_text = ''
    parse_response.clear()
    if request.POST:
        nlp_text = request.POST.get('text_body', None)
        if nlp_text:
            futures = [call_nlp_task(task,request, nlp_text) for task in tasks]
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            a, b = loop.run_until_complete(asyncio.wait(futures))
            loop.close()
        return JsonResponse(parse_response, safe=False)


