from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import re
import json
from entity_recognition.config import db_connection
from entity_recognition.pharser import skillPattern, buildPatterns, on_match
import spacy
from spacy.matcher import Matcher
from spacy.attrs import IS_PUNCT, LOWER
from spacy.matcher import PhraseMatcher

import requests

cur, conn = db_connection()
nlp_lg = spacy.load("en_core_web_lg")

nlp_sm = spacy.load("en_core_web_sm")


def CitybuildMatcher(patterns):
    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher

def LocationbuildMatcher(patterns):
    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher


def airlinebuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher

def flightClassbuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher

def SentenceMatcher(phrase_matcher, text):

    doc = nlp_sm((text.lower()))
    matches = phrase_matcher(doc)
    match_list =[]
    for b in matches:
        match_id, start, end = b
        match_list.append(str(doc[start: end]))
    return set(match_list)


def fetch_city(doc):

    if doc:
        cur.execute("""SELECT * FROM public.location;""")
        loc_names = cur.fetchall()
        c_name_list = []
        l_name_list = []
        city_list = []
        location_list = []
        sub_country_list = []
        for loc_name in loc_names:
            city_name = loc_name[0].rstrip().lower()
            location_name = loc_name[1].rstrip().lower()
            city_list.append(city_name)
            location_list.append(location_name)
            if loc_name[2]:
                sub_country = loc_name[2].rstrip().lower()
                sub_country_list.append(sub_country)

        c_patterns = buildPatterns(city_list)
        l_patterns = buildPatterns(set(location_list))
        s_patterns = buildPatterns(set(sub_country_list))

        city_matcher = CitybuildMatcher(c_patterns)
        loc_matcher = LocationbuildMatcher(l_patterns)
        sub_c_matcher = LocationbuildMatcher(s_patterns)

        c_name_list.extend(SentenceMatcher(city_matcher, doc))
        l_name_list.extend(SentenceMatcher(loc_matcher, doc))
        l_name_list.extend(SentenceMatcher(sub_c_matcher, doc))

        return c_name_list, l_name_list

def fetch_pnr(doc):
    pnr_list= []
    nlp_pnr = spacy.load("entity_recognition/models/pnr")
    doc = nlp_pnr(doc)
    for ent in doc.ents:
        if ent.label_ == 'PNR':
            pnr = re.findall(r'(\b[A-Z0-9]{6}\b)', ent.text)
            if pnr:
                    pnr_list.append(pnr[0])

    return pnr_list



def fetch_aircraft(doc):

    matcher = PhraseMatcher(nlp_sm.vocab, max_length=6)
    aircraft_list = []
    aircrafts =[]
    cur.execute("""SELECT * FROM public.aircraft;""")
    aircraft_codes = cur.fetchall()
    for aircraft_code in aircraft_codes:
            aircraft=aircraft_code[0].rstrip()
            aircraft_list.append(str(aircraft))
    air_patterns = [nlp_sm(text) for text in tuple(aircraft_list)]

    matcher = PhraseMatcher(nlp_sm.vocab)
    matcher.add('AIRCRAFT', None, *air_patterns)
    doc = nlp_sm(doc)
    matches = matcher(doc)
    for match_id, start, end in matches:
        rule_id = nlp_sm.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
        span = doc[start : end]  # get the matched slice of the doc
        aircrafts.append(span.text)
    return aircrafts


def fetch_airport(doc):

    match = re.findall(r"\b[A-Z][A-Z]+\b", doc)
    re_doc = ' '.join(match)
    doc = nlp_sm(re_doc)
    doc_string  = [token.text for token in doc if not token.is_stop]
    doc_text = nlp_sm(str(doc_string))

    matcher = Matcher(nlp_sm.vocab)
    cur.execute("""SELECT * FROM public.airport;""")
    airport_codes = cur.fetchall()
    air_code_array = []
    for airport_code in airport_codes:
        air_code_list = []
        air_code_list.append({'LOWER': re.sub("\s\s+", " ", airport_code[0]).rstrip().lower()})
        air_code_array.append(air_code_list)
    phrase_patterns = air_code_array
    matcher.add('Airport_Code', None, *phrase_patterns)
    matches = matcher(doc_text)
    airport_list = []
    for match_id, start, end in matches:
        span = doc_text[start:end]
        airport_list.append(span.text)
    return airport_list

def fetch_flight_no(doc):

    f_nos = re.findall(r'(\b([A-Z]{2})(?:\s?)(\d{2,4}\b))',doc)
    if f_nos:
        flight_numbers = list(zip(*f_nos))[0]
        flight_no_list = []
        cur.execute("""SELECT * FROM public.airline;""")
        airport_codes = cur.fetchall()
        for f_nos in flight_numbers:
            for a_code in airport_codes:
                if f_nos[:2] in a_code:
                    flight_no_list.append(f_nos)
        return(flight_no_list)
    else:
        return None


def fetch_awb(doc):

    f_nos = re.findall(r'(\d{3}[/|-|\s|–]?[-–/\s]?[/|-|\s]?\d{4}[-|\s|–]?\d{4})', doc)
    awb_no_list = []
    if f_nos:
        awb_no_list.append(f_nos)
        return(awb_no_list)
    else:
        return None


def fetch_airline(doc):

    doc_str = doc
    match = re.findall(r"\b[A-Z0-9][A-Z0-9]+\b", doc)
    re_doc = ' '.join(match)
    doc = nlp_sm(re_doc)
    doc_string = [token.text for token in doc if not token.is_stop]
    doc_text = nlp_sm(str(doc_string))
    airline_list = []
    airline_matcher = Matcher(nlp_sm.vocab)
    cur.execute("""SELECT * FROM public.airline;""")
    airline_codes = cur.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list.append({'LOWER': (airline_code[0]).lower()})
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].rstrip().lower()
            airline_name_array.append(airline_names)

    phrase_patterns = airline_code_array
    airline_matcher.add('Airline_Code', None, *phrase_patterns)
    airline_matches = airline_matcher(doc_text)
    for match_id, start, end in airline_matches:
        string_id = nlp_sm.vocab.strings[match_id]  # get string representation
        span = doc_text[start:end]  # the matched span
        airline_list.append(span.text)

    airline_patterns = buildPatterns(airline_name_array)

    airline_matcher = airlinebuildMatcher(airline_patterns)

    airline_list.extend(SentenceMatcher(airline_matcher, doc_str))
    return airline_list

def fetch_flight_class(doc):

    cur.execute("""SELECT * FROM public.flight_class;""")
    flight_class = cur.fetchall()
    flight_class_array = []

    for flight_cls in flight_class:
        flight_class_list = []
        if flight_cls[0]:
            airline_names = flight_cls[0].rstrip().lower()
            flight_class_array.append(airline_names)

    flight_cls_patterns = buildPatterns(flight_class_array)

    airline_matcher = flightClassbuildMatcher(flight_cls_patterns)

    flight_class_list.extend(SentenceMatcher(airline_matcher, doc))
    return flight_class_list



def fetch_date(doc):

    url = 'http://3.122.246.102:8000/parse/'
    payload = {}
    payload['text'] = doc
    payload['locale'] = 'en_DE'
    payload['tz'] = 'CET'

    duck_respose = requests.post(url, data=payload)
    dres_list = duck_respose.json()
    date_list = []
    date_body =[]
    if dres_list:
        for dat in dres_list:

            if dat['dim'] == 'time':
                date_text= dat['body']

                if date_text:
                    dt = date_text.replace("on", '').strip()

                date_body.append(dt.upper())
                try:
                    date_body.remove("BOM")
                except:
                    print("Error")
                grain = dat['value'].get('grain', None)
                if grain == 'day' or grain == 'month':
                    date_list.append(dat['value']['value'])
    return date_list,date_body


def fetch_from_to_date(doc):
    from_date_list = ''
    to_date_list = ''
    from_date_body =''
    to_date_body = ''
    nlp_date = spacy.load("entity_recognition/models/travel_date")
    doc = nlp_date(doc)
    for ent in doc.ents:
        if ent.label_ == 'FR_DATE':
            from_date_list,from_date_body=fetch_date(ent.text)
        elif ent.label_ == 'RE_DATE':
            to_date_list,to_date_body=fetch_date(ent.text)

    return from_date_list, to_date_list,from_date_body,to_date_body


def fetch_time(doc):
    doc = nlp_lg(doc)
    time_list = []
    for ent in doc.ents:
        if ent.label_ == 'TIME':
            time_list.append(ent.text)
    return time_list

def fetch_price(doc):
    doc = nlp_sm(doc)
    price_list = []
    for ent in doc.ents:
        if ent.label_ == 'MONEY':
            price_list.append(ent.text)
    return price_list

def fetch_quantity(doc):
    doc = nlp_sm(doc)
    quantity_list = []
    for ent in doc.ents:
        if ent.label_ == 'QUANTITY':
            quantity_list.append(ent.text)
    return quantity_list

def fetch_airstaff(doc):
    matcher = PhraseMatcher(nlp_sm.vocab, max_length=6)
    airstaff_list = []
    staffs = []
    cur.execute("""SELECT * FROM public.staff;""")
    staff_codes = cur.fetchall()
    for staff_code in staff_codes:
        aircraft = staff_code[0].rstrip()
        airstaff_list.append(str(aircraft))
    staff_patterns = [nlp_sm(text) for text in tuple(airstaff_list)]

    matcher = PhraseMatcher(nlp_sm.vocab)
    matcher.add('STAFF', None, *staff_patterns)
    doc = nlp_sm(doc)
    matches = matcher(doc)
    for match_id, start, end in matches:
        rule_id = nlp_sm.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
        span = doc[start: end]  # get the matched slice of the doc
        staffs.append(span.text)
    return staffs



def fetch_aviation_entities(doc):
    matcher = PhraseMatcher(nlp_sm.vocab, max_length=6)
    air_entities_list = []
    entities = []
    cur.execute("""SELECT * FROM public.aviation_entities;""")
    entities_codes = cur.fetchall()
    for entities_code in entities_codes:
        aircraft = entities_code[0].rstrip()
        air_entities_list.append(str(aircraft))
    staff_patterns = [nlp_sm(text) for text in tuple(air_entities_list)]

    matcher = PhraseMatcher(nlp_sm.vocab)
    matcher.add('ENTITIES', None, *staff_patterns)
    doc = nlp_sm(doc)
    matches = matcher(doc)
    for match_id, start, end in matches:
        rule_id = nlp_sm.vocab.strings[match_id]  # get the unicode ID, i.e. 'COLOR'
        span = doc[start: end]  # get the matched slice of the doc
        entities.append(span.text)
    return entities

def fetch_org(doc):

    doc = nlp_sm(doc)
    org_list = []
    for ent in doc.ents:
        if ent.label_ == 'ORG':
            org_list.append((ent.text).strip())
    return org_list

def fetch_person(doc):

    doc = nlp_sm(doc)
    person_list = []
    for ent in doc.ents:
        if ent.label_ == 'PERSON':
            person_list.append(ent.text)
    return person_list

def fetch_haul(doc):
    haul = re.findall(r'long[\s+,\-=#]+haul|short[\s+,\-=#]+haul|medium[\s+,\-=#]+haul', doc)
    haul_list = []
    if haul:
        haul_list.append(haul)
        return(haul_list)
    else:
        return None



@csrf_exempt
def entity_recognition(request,text_body=None):

    nlp_text=''
    parse_response = {}
    if request.POST:
        nlp_text = request.POST.get('text_body', None)

    else:
        nlp_text=text_body
    if nlp_text:

        flight_pnr = fetch_pnr(nlp_text)
        if flight_pnr:
            parse_response['PNR'] = flight_pnr
        flight_no= fetch_flight_no(nlp_text)
        if flight_no:
             if flight_no and flight_pnr:
                 flight_no = set(flight_no)-set(flight_pnr)
             parse_response['Flight_No'] = list(flight_no)
        airline_code =fetch_airline(nlp_text)
        if airline_code:
            air_lists = []
            if flight_no:
                for fligts in flight_no:
                    for air_code in airline_code:
                        if air_code in fligts:
                            air_lists.append(air_code)
            airline_names_list =   set(airline_code) - set(air_lists)
            if airline_names_list:
                parse_response['Airline'] = list(airline_names_list)

        city,location= fetch_city(nlp_text)

        from_date, to_date, from_date_body, to_date_body = fetch_from_to_date(nlp_text)
        if from_date_body or to_date_body:
            for city_val in city:
                for from_date_val in from_date_body:
                    if city_val in from_date_val.upper():
                        if city_val.upper() in city:
                             city.remove(city_val.upper())
                for from_date_val in to_date_body:
                    if city_val in from_date_val.upper():
                        if city_val.upper() in city:
                            city.remove(city_val.upper())
            for loc_val in location:
                for to_date_val in from_date_body:
                    if loc_val in to_date_val.upper():
                        if loc_val.upper() in location:
                            location.remove(loc_val.upper())
                for to_date_val in to_date_body:
                    if loc_val in to_date_val.upper():
                        if loc_val.upper() in location:
                            location.remove(loc_val.upper())
        if location:
            loc = set(location) - set(city)
            if loc:
                loc = [x.upper() for x in loc]
                parse_response['Location'] = list(loc)
        city = [x.upper() for x in city]
        dates,date_body=fetch_date(nlp_text)

        # if dates:
        #     parse_response['Dates'] =dates


        if date_body or from_date_body or to_date_body:
            date_text_only = set(date_body) - set(from_date_body)
            date_text_only = date_text_only - set(to_date_body)
            if date_text_only:
                parse_response['Dates_Text'] = list(date_text_only)
        if from_date:
            parse_response['Onward_Date'] = from_date
            parse_response['Onward_Date_Desc'] = from_date_body
        if to_date:
            parse_response['Return_Date'] = to_date
            parse_response['Return_Date_Desc'] = to_date_body
        prices = fetch_price(nlp_text)

        if prices:
                parse_response['Price'] = prices
        times = fetch_time(nlp_text)
        if times:
            parse_response['Times'] = times
        awb_no = fetch_awb(nlp_text)
        if awb_no:
            parse_response['AWBN'] = awb_no

        aircraft = fetch_aircraft(nlp_text)
        if aircraft:
            parse_response['Aircraft'] = aircraft

        air_staff = fetch_airstaff(nlp_text)
        if air_staff:
            parse_response['Staff'] = air_staff

        air_entities = fetch_aviation_entities(nlp_text)
        if air_entities:
            parse_response['Entities'] = air_entities

        # person_names = fetch_person(nlp_text)
        # if person_names:
        #     parse_response['Person'] = person_names

        quantity = fetch_quantity(nlp_text)
        if quantity:
            parse_response['Quantity'] = quantity

        flight_class = fetch_flight_class(nlp_text)
        if flight_class:
            parse_response['Flight_Class'] = flight_class

        airport_code = fetch_airport(nlp_text)

        if from_date_body or to_date_body:
            airport_code_rm_list =[]
            for airport_code_val in airport_code:
                for from_date_val in from_date_body:
                    if airport_code_val in from_date_val.upper():
                        if airport_code_val.upper() in airport_code:
                            airport_code.remove(airport_code_val.upper())
                for to_date_body_vals in to_date_body:
                    if airport_code_val in to_date_body_vals.upper():
                        if airport_code_val.upper() in airport_code:
                            airport_code.remove(airport_code_val.upper())
        org_names = fetch_org(nlp_text)
        if org_names:
            if airport_code:
                org_names = set(org_names) - set(airport_code)
                if flight_no:
                    org_names = set(org_names) - set(flight_no)
                # if org_names:
                #     parse_response['Organization'] = list(org_names)
        fr_air =' '
        to_air = ''
        if airport_code or city or location:
            sour_dest = airport_code + city

            string_pattern = '(' + '|'.join(sour_dest) + ')'

            from_airport = re.findall(r'(From|from|FROM)\s*{0}'.format(str(string_pattern).lower()), nlp_text.lower())
            to_airport = re.findall(r'(to|-|To|TO)\s*{0}'.format(str(string_pattern).lower()), nlp_text.lower())
            if from_airport:
                fr_air= [fr[1].upper() for fr in from_airport]
            if to_airport:
                to_air = [to[1].upper() for to in to_airport]
        fr_air= [str for str in fr_air if str.strip()]
        to_air = [str for str in to_air if str.strip()]
        if fr_air:

            parse_response['Source_Airport'] = fr_air
        if to_air:
            parse_response['Destination_Airport'] = to_air


        if fr_air or to_air or  airport_code  or city:
            fr_air=list(fr_air)
            to_air = list(to_air)
            air_source= set(airport_code) - (set(to_air + fr_air + city))
            air_dest= set(city) - set(to_air + fr_air + airport_code)
            if  air_dest:
                parse_response['City']  = list(air_dest)
            if air_source:
                parse_response['Airport'] = list(air_source)

        list_haul = fetch_haul(nlp_text);
        if list_haul:
            parse_response['Haul'] = list_haul
    if text_body:
        return parse_response
    else:
        return JsonResponse(parse_response, safe=False)


@csrf_exempt
def city_aircode(request,text_body=None):
    aircode_dict = {}
    source_list = []
    dest_list = []
    if request.POST:
        source_airport = request.POST.get('source_airport', None)
        dest_airport = request.POST.get('dest_airport', None)
        air_code_check_source = re.findall(r'(\b[A-Z]{3}\b)', source_airport)
        air_code_check_dest = re.findall(r'(\b[A-Z]{3}\b)', dest_airport)
        if air_code_check_source:
            aircode_dict["source_airport_code"]= air_code_check_source[0]
        if air_code_check_dest:
            aircode_dict["destination_airport_code"] = air_code_check_dest[0]
        if source_airport:
            source_airport = source_airport.upper()
            cur.execute("""SELECT * FROM public.city_aircode_lh where UPPER(city_name) = %s;""",(source_airport,))
            source_airlist = cur.fetchall()
            for source_air in source_airlist:
                source = source_air
                if source:
                    aircode_dict["source_airport_code"] = source[1]
        if dest_airport:
            dest_airport = dest_airport.upper()
            cur.execute("""SELECT * FROM public.city_aircode_lh where UPPER(city_name) = %s;""", (dest_airport,))
            dest_airlist = cur.fetchall()
            for dest_air in dest_airlist:
                dest = dest_air
                if dest:
                    aircode_dict["destination_airport_code"] = dest[1]
    return JsonResponse(aircode_dict, safe=False)




def fetch_date_cargo(doc):

    url = 'http://3.122.246.102:8000/parse/'
    payload = {}
    payload['text'] = doc
    payload['locale'] = 'en_DE'
    payload['tz'] = 'CET'

    duck_respose = requests.post(url, data=payload)
    dres_list = duck_respose.json()
    date_list = []
    date_body =[]
    if dres_list:
        for dat in dres_list:

            if dat['dim'] == 'time':
                date_text = dat['body']
                if  dat['value'].get('values', None):
                    date_list = dat['value']['values']
                if date_text:
                    dt = date_text.replace("on", '').strip()

                date_body.append(dt.upper())
    return date_list,date_body

@csrf_exempt
def entity_recognition_cargo(request,text_body=None):
    print("entity_recognition_cargo")
    nlp_text=''
    parse_response = {}
    if request.POST:
        nlp_text = request.POST.get('text_body', None)

    else:
        nlp_text=text_body
    if nlp_text:

        flight_pnr = fetch_pnr(nlp_text)
        if flight_pnr:
            parse_response['PNR'] = flight_pnr
        flight_no= fetch_flight_no(nlp_text)
        if flight_no:
             if flight_no and flight_pnr:
                 flight_no = set(flight_no)-set(flight_pnr)
             parse_response['Flight_No'] = list(flight_no)
        # airline_code =fetch_airline(nlp_text)
        airline_code =''
        if airline_code:
            air_lists = []
            if flight_no:
                for fligts in flight_no:
                    for air_code in airline_code:
                        if air_code in fligts:
                            air_lists.append(air_code)
            airline_names_list =   set(airline_code) - set(air_lists)
            if airline_names_list:
                parse_response['Airline'] = list(airline_names_list)

        city,location= fetch_city(nlp_text)

        from_date, to_date, from_date_body, to_date_body = fetch_from_to_date(nlp_text)
        if from_date_body or to_date_body:
            for city_val in city:
                for from_date_val in from_date_body:
                    if city_val in from_date_val.upper():
                        if city_val.upper() in city:
                             city.remove(city_val.upper())
                for from_date_val in to_date_body:
                    if city_val in from_date_val.upper():
                        if city_val.upper() in city:
                            city.remove(city_val.upper())
            for loc_val in location:
                for to_date_val in from_date_body:
                    if loc_val in to_date_val.upper():
                        if loc_val.upper() in location:
                            location.remove(loc_val.upper())
                for to_date_val in to_date_body:
                    if loc_val in to_date_val.upper():
                        if loc_val.upper() in location:
                            location.remove(loc_val.upper())
        if location:
            loc = set(location) - set(city)
            if loc:
                loc = [x.upper() for x in loc]
                parse_response['Location'] = list(loc)
        city = [x.upper() for x in city]
        dates,date_body=fetch_date_cargo(nlp_text)

        if dates:
            parse_response['Dates_info'] =dates


        if date_body or from_date_body or to_date_body:
            date_text_only = set(date_body) - set(from_date_body)
            date_text_only = date_text_only - set(to_date_body)
            if date_text_only:
                parse_response['Dates_Text'] = list(date_text_only)
        prices = fetch_price(nlp_text)

        if prices:
                parse_response['Price'] = prices
        times = fetch_time(nlp_text)
        if times:
            parse_response['Times'] = times
        awb_no = fetch_awb(nlp_text)
        if awb_no:
            parse_response['AWBN'] = awb_no

        aircraft = fetch_aircraft(nlp_text)
        if aircraft:
            parse_response['Aircraft'] = aircraft

        air_staff = fetch_airstaff(nlp_text)
        if air_staff:
            parse_response['Staff'] = air_staff

        air_entities = fetch_aviation_entities(nlp_text)
        if air_entities:
            parse_response['Entities'] = air_entities

        # person_names = fetch_person(nlp_text)
        # if person_names:
        #     parse_response['Person'] = person_names

        quantity = fetch_quantity(nlp_text)
        if quantity:
            parse_response['Quantity'] = quantity

        flight_class = fetch_flight_class(nlp_text)
        if flight_class:
            parse_response['Flight_Class'] = flight_class
        airport_code = fetch_airport(nlp_text)
        if from_date_body or to_date_body:
            airport_code_rm_list = []
            for airport_code_val in airport_code:
                for from_date_val in from_date_body:
                    if airport_code_val in from_date_val.upper():
                        if airport_code_val.upper() in airport_code:
                            airport_code.remove(airport_code_val.upper())
                for to_date_body_vals in to_date_body:
                    if airport_code_val in to_date_body_vals.upper():
                        if airport_code_val.upper() in airport_code:
                            airport_code.remove(airport_code_val.upper())
        org_names = fetch_org(nlp_text)
        if org_names:
            if airport_code:
                org_names = set(org_names) - set(airport_code)
                if flight_no:
                    org_names = set(org_names) - set(flight_no)
                    # if org_names:
                    #     parse_response['Organization'] = list(org_names)
        fr_air = ' '
        to_air = ''
        if airport_code or city or location:
            sour_dest = airport_code + city

            string_pattern = '(' + '|'.join(sour_dest) + ')'

            from_airport = re.findall(r'(From|from|FROM)\s*{0}'.format(str(string_pattern).lower()), nlp_text.lower())
            to_airport = re.findall(r'(to|-|To|TO)\s*{0}'.format(str(string_pattern).lower()), nlp_text.lower())
            if from_airport:
                fr_air = [fr[1].upper() for fr in from_airport]
            if to_airport:
                to_air = [to[1].upper() for to in to_airport]
        fr_air = [str for str in fr_air if str.strip()]
        to_air = [str for str in to_air if str.strip()]
        if fr_air:
            parse_response['Source_Airport'] = fr_air
        if to_air:
            parse_response['Destination_Airport'] = to_air

        if fr_air or to_air or airport_code or city:
            fr_air = list(fr_air)
            to_air = list(to_air)
            air_source = set(airport_code) - (set(to_air + fr_air + city))
            air_dest = set(city) - set(to_air + fr_air + airport_code)
            if air_dest:
                parse_response['City'] = list(air_dest)
            if air_source:
                parse_response['Airport'] = list(air_source)

        list_haul = fetch_haul(nlp_text);
        if list_haul:
            parse_response['Haul'] = list_haul

        if text_body:
            return parse_response
        else:
             return JsonResponse(parse_response, safe=False)
