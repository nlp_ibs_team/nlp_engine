from __future__ import unicode_literals, print_function

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding


# new entity label
LABEL = 'PNR'

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
    ("J6CVGL is my Booking reference no (PNR)", {'entities': [(0, 6, 'PNR')]}),
    ("Continental Confirmation Number: BQ6FHS", {'entities': [(33, 39, 'PNR')]}),
    ("SpiceJet Booking PNR L5BCYV: 24 Feb 2017 Kochi-Hyderabad for MS. SHARATH", {'entities': [(21, 27, 'PNR')]}),
    ("my Booking reference is J6C5VL", {'entities': [(24, 30, 'PNR')]}),
    ("my Booking reference is G6CVVG", {'entities': [(24, 30, 'PNR')]}),
    ("my Booking reference is K6CVM8", {'entities': [(24, 30, 'PNR')]}),
    ("my Booking reference was M6LCV3", {'entities': [(25, 31, 'PNR')]}),
    ("my Booking reference was T6CVH1", {'entities': [(25, 31, 'PNR')]}),
    ("my Booking reference was B6CNHT", {'entities': [(25, 31, 'PNR')]}),
    ("Your PNR is KFBZQG.", {'entities': [(12, 18, 'PNR')]}),
    ("Your PNR is MU22A6.", {'entities': [(12, 18, 'PNR')]}),
    ("Your PNR is 6P4RV7.", {'entities': [(12, 18, 'PNR')]}),
    ("Your PNR is U7223N.", {'entities': [(12, 18, 'PNR')]}),
    ("my booking PNR is D3AADX.", {'entities': [(12, 24, 'PNR')]}),
    ("my booking PNR bumber is 6JTSVB.", {'entities': [(25, 31, 'PNR')]}),
    ("PNR DZ2N22.", {'entities': [(4, 10, 'PNR')]}),
    ("PNR:F9QQ2A.", {'entities': [(4, 10, 'PNR')]}),
    ("PNR-L56H24.", {'entities': [(4, 10, 'PNR')]}),
    ("reference no DXS2AQ", {'entities': [(13, 19, 'PNR')]}),
    ("reference no is DXS2AQ", {'entities': [(16, 21, 'PNR')]}),
    ("passenger name record is 71Y908", {'entities': [(25, 31, 'PNR')]}),
    ("passenger name record 6YV3G5", {'entities': [(22, 28, 'PNR')]}),
    ("Unable to claim missing miles from my October trip from Kochi-Chicago on 07 Oct and  return trip from Chicago-Kochi on 21st Oct 2015,my Booking reference was M6LCV3", {'entities': [(158, 164, 'PNR')]}),
    ("trip from Kochi-Chicago with PNR M6LCRW on 07 Oct and  return trip from Chicago-Kochi on 21st Oct 2015", {'entities': [(33, 39, 'PNR')]}),
    ("I have travel with PNR QM6YTK trip from dubai to Kochi on 21st Oct 2015.", {'entities': [(23, 29, 'PNR')]}),
    ("Travel in 2 hour short-haul and long haul with pnr W6ETY9 on 21-dec  pre year.", {'entities': [(51, 57, 'PNR')]}),
    ("Ashok_Gajapathi @moca_goi @IndiGo6E @AdityaGhosh6E  PNR is T9KP2S. IndiGo calling me 3hours before departure to inform me flight delay 1", {'entities': [(59, 65, 'PNR')]}),
    ("Ashok_Gajapathi @moca_goi @IndiGo6E @AdityaGhosh6E  PNR is DXS2AQ. ", {'entities': [(59, 65, 'PNR')]}),
    ("For our second flight on Qantas Airlines, we were flying QF686 from Adelaide (ADL) to Melbourne/Tullamarine (MEL), prior to returning to the USA, after two weeks in Australia. Our ship was a 737-800 V1, number VH-VXF, and again, we were in Row 20, seats D&E, in Economy.Out ticket reference was JH4539", {'entities': [(295, 301, 'PNR')]}),
    ("we were in Row 20, seats D&E, in Economy.Out ticket reference was JH4539", {'entities': [(66, 72, 'PNR')]}),
    ("we were in Row 20, seats D&E, in Economy.Out ticket reference was WEl90B.", {'entities': [(66, 72, 'PNR')]}),
    ("PNR is T9KP2S. IndiGo calling me 3hours before departure to inform me flight delay", {'entities': [(7, 13, 'PNR')]}),
    ("My PNR is T9KP2S.IndiGo calling me 3hours before departure to inform me flight delay", {'entities': [(10, 16, 'PNR')]}),
    ("Hi ElaineThank you for your prompt reply, however it is not the refund I am querying. I know I have received the refund for VP918P on 7th Nov, but there should be another refund for T9U60W because there were 2 flights for 2 people that were cancelled hence 2 different refund reference numbers. Would you please tell me when can I expect the refund for T9U60W?Thank you for your kind assistance", {'entities': [(124, 130, 'PNR'),(182, 188, 'PNR'),(353, 359, 'PNR')]}),
    ("my booking PNR are D3AADX for COK to-LHR and 6RR1ZS for LHR-COK. ", {'entities': [(19, 25, 'PNR'),(45, 51, 'PNR')]}),
    ("My booking PNR D3AADX for COK to-LHR and 6RR1ZS for LHR-COK.", {'entities': [(15, 21, 'PNR'),(41, 47, 'PNR')]}),
    ("I know I have received the refund of VH918P on 7th Nov, but there should be another refund for DF6A22 because there were 2 flights for 2 people that were cancelled hence 2 different refund reference numbers. Would you please tell me when can I expect the refund for T4R60B?Thank you for your kind assistance", {'entities': [(95, 101, 'PNR'), (266, 272, 'PNR')]}),
    ("I know I have received the refund of VH918P on 7th Nov, but there should be another refund for DF6A22 because there were 2 flights for 2 people that were cancelled hence 2 different refund reference numbers. Would you please tell me when can I expect the refund for WE8YT6 the status.", {'entities': [(95, 101, 'PNR')]}),
    ("My booking PNR D3AADX for COK to-LHR and 6RR1ZS for LHR-COK.", {'entities': [(25, 31, 'PNR'), (36, 42, 'PNR')]}),
    ("Your PNR is KFBZQG.", {'entities': [(12, 18, 'PNR'),(23, 29, 'PNR')]}),
    ("PNR is T9KP2S. IndiGo calling me 3hours before departure to inform me flight delay 1 For our second flight on Qantas Airlines.", {'entities': [(7, 13, 'PNR')]}),
    ("PNR is QA3RV5. IndiGo Airline", {'entities': [(7, 13, 'PNR')]}),
    ("PNR is T9KP2S. IndiGo calling me 3hours before departure to inform me flight delay 1/2.", {'entities': [(7, 13, 'PNR')]}),
    ("Gajapathi's PNR is M9KP2S IndiGo calling me 3hours before departure to inform me flight delay 1 2.", {'entities': [(19, 25, 'PNR')]}),

]


@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)


def train_entity(model=None, new_model_name='pnr', output_dir=None, n_iter=10):
    """Set up the pipeline and entity recognizer, and train the new entity."""

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.load('en_core_web_sm')  # create blank Language class
        print("Created blank 'en' model")
    # Add entity recognizer to model if it's not in the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner)
    # otherwise, get it, so we can add labels to it
    else:
        ner = nlp.get_pipe('ner')

    ner.add_label(LABEL)   # add new entity label to entity recognizer
    if model is None:
        optimizer = nlp.begin_training()
    else:
        # Note that 'begin_training' initializes the models, so it'll zero out
        # existing entity types.
        optimizer = nlp.entity.create_optimizer()

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.35,
                           losses=losses)
            print('Losses', losses)

    # test the trained model
    test_text = "Gajapathi's PNR is M9KP2S IndiGo calling me 3hours before departure to inform me flight delay 1 2."
    doc = nlp(test_text)
    print("Entities in '%s'" % test_text)
    for ent in doc.ents:
        print(ent.label_, ent.text)

    # save model to output directory
    print('output_dir')
    output_dir ='models/pnr'
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.meta['name'] = new_model_name  # rename model
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        for ent in doc2.ents:
            print(ent.label_, ent.text)

if __name__ == '__main__':
    plac.call(train_entity)

