import psycopg2

def db_connection():
    conn = psycopg2.connect("dbname='nlpenginedb' user='postgres' password='qwerty' host='localhost'")
    cur = conn.cursor()
    return cur, conn

