
def skillPattern(skill):
    pattern = []
    for b in skill.split():
        pattern.append({'LOWER':b})
    return pattern

def buildPatterns(skills):
    pattern = []
    for skill in skills:
        pattern.append(skillPattern(skill))
    return list(zip(skills, pattern))

def on_match(matcher, doc, id, matches):
    return matches