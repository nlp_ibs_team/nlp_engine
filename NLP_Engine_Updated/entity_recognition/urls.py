from django.conf.urls import url
from .views import entity_recognition,city_aircode,entity_recognition_cargo
urlpatterns = [
    url(r'^entity_recognition/', entity_recognition, name="entity_recognition"),
    url(r'^city_to_code/', city_aircode, name="city_to_code"),
    url(r'^entity_recognition_cargo/', entity_recognition_cargo, name="entity_recognition_cargo_cargo")

]