from django.conf.urls import url
from .views import aws_sns
urlpatterns = [

    url(r'^sns-alert/', aws_sns, name="aws_sns"),
]