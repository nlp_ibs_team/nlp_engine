from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import decomposition, ensemble
import xgboost
from operator import itemgetter
import pandas as pd, numpy, string
import re, string, unicodedata
from bs4 import BeautifulSoup
import pickle
import psycopg2

conn = psycopg2.connect("dbname='nlp_engine' user='postgres' password='manager123' host='localhost'")
cur = conn.cursor()

df= pd.read_sql_query('select * from "Intent_Classification_Mail"',con=conn)

# df = pd.read_csv('csv_files/Intent_Classification_Mail_Bot.csv')
train_x, test_x, train_y, test_y = model_selection.train_test_split(df['text'], df['label'].map(lambda x: x.strip()),random_state=42)
model_array = []


# def strip_html(text):
#     soup = BeautifulSoup(text, "html.parser")
#     return soup.get_text()

def remove_pre_tag(text):
    return re.sub('<pre>', '', text)
def remove_equals_tag(text):
    return re.sub('=', '', text)
def greater_than_sym(text):
    return re.sub('&gt;', '', text)
def remove_less_than_tag(text):
    return re.sub('&lt', '', text)
def get_just_email(text):
    return text.split("From: Qantas Frequent Flyer")[0]

def denoise_text(text):
    text = remove_pre_tag(text)
    text = remove_equals_tag(text)
    text= greater_than_sym(text)
    text = get_just_email(text)
    text = remove_less_than_tag(text)
    return text
pd.options.display.max_colwidth=150
df['text']= df['text'].apply(denoise_text)
df.to_csv("edited_file.csv", sep=',', encoding='utf-8')



def extract_features(mode, analyser, ngrm_range, X_train, X_test):
    if mode == 'Count':
        feature_vector = CountVectorizer(analyzer=analyser, ngram_range=ngrm_range)
    elif mode == 'Tfidf':
        feature_vector = TfidfVectorizer(analyzer=analyser, ngram_range=ngrm_range)

    feature_vector.fit(df['text'])
    X_train_vector = feature_vector.fit_transform(X_train)
    X_test_vector = feature_vector.transform(X_test)

    return [X_train_vector, X_test_vector, feature_vector]


def train_model(classifier,feature_vector_train,train_y,feature_vector_test,test_y):
    classifier.fit(feature_vector_train,train_y)
    predictions = classifier.predict(feature_vector_test)
    return [classifier,metrics.accuracy_score(predictions,test_y)]


def addAllToModelsToList(model, vectorizer, accuracy):
    model_details = {
        'model': model,
        'vectorizer': vectorizer,
        'accuracy': accuracy
    }
    model_array.append(model_details)


def test_model(classifierType,vectorizerMode,analyserType,nGramType,train_x,test_x,train_y,test_y):
    feature_vectors = extract_features(vectorizerMode,analyserType,nGramType,train_x,test_x)
    model_output = train_model(classifierType,feature_vectors[0],train_y,feature_vectors[1],test_y)
    model = model_output[0]
    accuracy = model_output[1]
    if accuracy >= 0.80 :
       # print(model.predict(feature_vectors[2].transform(["This is too much.I had a two hour wait in London and you forgot my baggage."])))
       # print ("%s, %s %s %s Vectors" %(str(classifierType).split("(")[0],vectorizerMode,analyserType,nGramType), accuracy)
        addAllToModelsToList(model,feature_vectors[2],accuracy)





def choose_all_models_mail_bot():
    print('Testing models using Naive Bayes Classifier')
    test_model(naive_bayes.MultinomialNB(),'Count','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Count','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Count','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Count','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Count','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Count','char_wb',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','char_wb',{2,3},train_x,test_x,train_y,test_y)
    test_model(naive_bayes.MultinomialNB(),'Tfidf','char_wb',{3,4},train_x,test_x,train_y,test_y)
    print('Testing models using Logistic Regression')
    test_model(linear_model.LogisticRegression(),'Count','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Count','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Count','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Count','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Count','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Count','char_wb',{2,3},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(linear_model.LogisticRegression(),'Tfidf','char_wb',{2,3},train_x,test_x,train_y,test_y)
    print('Testing models using Random Forest Classifier')
    test_model(ensemble.RandomForestClassifier(),'Count','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Count','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Count','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Count','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Count','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Count','char_wb',{2,3},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(ensemble.RandomForestClassifier(),'Tfidf','char_wb',{2,3},train_x,test_x,train_y,test_y)
    print('Testing models using Boosting Model')
    test_model(xgboost.XGBClassifier(),'Count','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Count','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Count','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','word',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','char_wb',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','char',{3,4},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Count','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Count','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Count','char_wb',{2,3},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','word',{2,3},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','char',{2,3},train_x,test_x,train_y,test_y)
    test_model(xgboost.XGBClassifier(),'Tfidf','char_wb',{2,3},train_x,test_x,train_y,test_y)
    print('Printing Model Array')
    print(len(model_array))
    print('Sorting Array of Models based on Accuracy')
    sorted_list = sorted(model_array , key=itemgetter('accuracy'),reverse = True)
    with open('/Users/a-7624/Documents/indent_classifiers.models', 'wb') as handle:
        pickle.dump(sorted_list, handle, protocol=pickle.HIGHEST_PROTOCOL)




df_chat = pd.read_sql_query('select * from "Intent_Classification_Chat"', con=conn)
# train_x, test_x, train_y, test_y = model_selection.train_test_split(df['text'],df['label'].map(lambda x: x.strip()),random_state=50)

train_x, test_x, train_y, test_y = model_selection.train_test_split(df_chat['text'], df_chat['label'].map(lambda x: x.strip()), random_state=50)
model_array = []

def choose_all_models_chat_bot():

    test_model(naive_bayes.MultinomialNB(), 'Count', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Count', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Count', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Count', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Count', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Count', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(naive_bayes.MultinomialNB(), 'Tfidf', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    print('Testing models using Logistic Regression')
    test_model(linear_model.LogisticRegression(), 'Count', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Count', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Count', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Count', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Count', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Count', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(linear_model.LogisticRegression(), 'Tfidf', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    print('Testing models using Random Forest Classifier')
    test_model(ensemble.RandomForestClassifier(), 'Count', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Count', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Count', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Count', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Count', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Count', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(ensemble.RandomForestClassifier(), 'Tfidf', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    print('Testing models using Boosting Model')
    test_model(xgboost.XGBClassifier(), 'Count', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Count', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Count', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'word', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'char_wb', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'char', {1, 2}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Count', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Count', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Count', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'word', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'char', {2, 3}, train_x, test_x, train_y, test_y)
    test_model(xgboost.XGBClassifier(), 'Tfidf', 'char_wb', {2, 3}, train_x, test_x, train_y, test_y)
    print('Printing Model Array')
    print('Sorting Array of Models based on Accuracy')
    sorted_list = sorted(model_array, key=itemgetter('accuracy'), reverse=True)

    with open('/Users/a-7624/Documents/indent_classifiers_chat_bot.models', 'wb') as handle:
        pickle.dump(sorted_list, handle, protocol=pickle.HIGHEST_PROTOCOL)


def run_prediction_tests(isLoadReq):

    with open('/Users/a-7624/Documents/indent_classifiers.models' , 'rb') as pickle_file:
        models_list = pickle.load(pickle_file)
    df = pd.read_csv('csv_files/Test_Data_Mail_Bot.csv')
    # if isLoadReq:
    #     models_list = choose_all_models()
    # else:
    #     models_list = model_list
    df['text'] = df['text'].apply(denoise_text)
    for i in range(5):
        predicted_value = models_list[i]["model"].predict(models_list[i]["vectorizer"].transform(df['text']))
        print("Prediction Using %s model, Vectoriser %s." % (
        str(models_list[i]["model"]).split("(")[0], str(models_list[i]["vectorizer"]).split("(")[0]))
        print("Predicted Value is : %s" % predicted_value)


