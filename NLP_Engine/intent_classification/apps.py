from django.apps import AppConfig


class IntentClassificationConfig(AppConfig):
    name = 'intent_classification'
