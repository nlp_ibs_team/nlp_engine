from spellchecker import SpellChecker

spell = SpellChecker()  # loads default word frequency list
spell.word_frequency.load_text_file('spell_checker_list')

# if I just want to make sure some words are not flagged as misspelled

misspelled =['lufthansa']
for word in misspelled:
    # Get the one `most likely` answer
    print(spell.correction(word))


