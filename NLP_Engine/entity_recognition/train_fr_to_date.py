from __future__ import unicode_literals, print_function

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding


# new entity label
LABEL_FR = 'FR_DATE'
LABEL_TO = 'RE_DATE'

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
    ("Unable to claim missing miles from my October trip from Kochi-Chicago on 07 Oct and  return trip from Chicago-Kochi on 21st Oct 2015", {'entities': [(73, 79, 'FR_DATE'),(119, 132, 'RE_DATE')]}),
    ("travel from chennai to bangalore on Friday", {'entities': [(36, 42, 'FR_DATE')]}),
    ("I want to book a flight from chennai to bangalore for tomorrow", {'entities': [(54, 62, 'FR_DATE')]}),
    ("trip from Chicago-Kochi on 21st Oct 2015", {'entities': [(27, 40, 'FR_DATE')]}),
    ("Travel on 07 Oct", {'entities': [(10, 16, 'FR_DATE')]}),
    ("My trip is on 6-8-2019", {'entities': [(25, 31, 'FR_DATE')]}),
    ("My travel plan is on next sunday and return on 25-03-2019.", {'entities': [(26, 32, 'FR_DATE'),(47, 57, 'RE_DATE')]}),
    ("Travel on 07 Oct and return 09 Oct", {'entities': [(10, 16, 'FR_DATE'),(28, 34, 'RE_DATE')]}),
    ("Travel on 15 Oct and return 21 Oct", {'entities': [(10, 16, 'FR_DATE'), (28, 34, 'RE_DATE')]}),
    ("Travel on 8th Oct and return 15st Oct", {'entities': [(10, 17, 'FR_DATE'), (29, 37, 'RE_DATE')]}),
    ("Unable to claim missing miles from my October trip from Kochi-Chicago on 07 Oct and  return trip from Chicago-Kochi on 21st Oct 2019", {'entities': [(73, 79, 'FR_DATE'), (119, 132, 'RE_DATE')]}),
    ("We flew from OR Tambo to Sydney on the 14th April and had the best flight I have had to Sydney.", {'entities': [(39, 49, 'FR_DATE')]}),
    ("I want to book a flight from chennai to bangalore for tomorrow", {'entities': [(54, 62, 'FR_DATE')]}),
    ("trip from Chicago-Kochi on next friday", {'entities': [(32, 38, 'FR_DATE')]}),
    ("I want to travel from London to Berlin on 08-06-2019.", {'entities': [(42, 52, 'FR_DATE')]}),
    ("I want to book a flight from Sydney to Haneda onward 4th April 2019 and return on 10th April  2019", {'entities': [(53, 67, 'FR_DATE'),(82, 98, 'RE_DATE')]}),
    ("Can you please reserve a ticket to Paris on 14th May?", {'entities': [(44, 52, 'FR_DATE')]}),
    ("from Tokyo to Newyork on 18-Apr-2019 returning on 24-Apr-2019", {'entities': [(25, 36, 'FR_DATE'),(50, 61, 'RE_DATE')]}),
    ("From Tokyo to Newyork on 11-Feb-2019 returning on 24-Feb-2019.",{'entities': [(25, 36, 'FR_DATE'), (50, 61, 'RE_DATE')]}),
    ("My returning journey is on 24-Feb-2019.",{'entities': [(27, 38, 'RE_DATE')]}),
    ("I am returning on 24-Apr-2019.", {'entities': [(18, 29, 'RE_DATE')]}),
    ("Please book a ticket from mumbai to bangalore on 24-feb-2019", {'entities': [(49, 60, 'FR_DATE')]}),
    ("Please book a ticket from mumbai to bangalore on 24-feb-2019 and return on 29-feb-2019", {'entities': [(49, 60, 'FR_DATE'),(75, 86, 'RE_DATE')]}),
    ("book a ticket from COK to DXB on 24-feb-2019 back on 3ed Mar.", {'entities': [(33, 44, 'FR_DATE'), (53, 60, 'RE_DATE')]}),
    ("book from MAN to LHR on 24-April and  back on 30 April.", {'entities': [(24, 32, 'FR_DATE'),(46, 54, 'RE_DATE')]}),
]


@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)


def train_entity(model=None, new_model_name='travel_date', output_dir=None, n_iter=10):
    """Set up the pipeline and entity recognizer, and train the new entity."""

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.load('en')  # create blank Language class
        print("Created blank 'en' model")
    # Add entity recognizer to model if it's not in the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner)
    # otherwise, get it, so we can add labels to it
    else:
        ner = nlp.get_pipe('ner')

    ner.add_label(LABEL_FR)   # add new entity label to entity recognizer
    ner.add_label(LABEL_TO)
    if model is None:
        optimizer = nlp.begin_training()
    else:
        # Note that 'begin_training' initializes the models, so it'll zero out
        # existing entity types.
        optimizer = nlp.entity.create_optimizer()

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.35,
                           losses=losses)
            print('Losses', losses)

    # test the trained model
    test_text = 'from chennai to bangalore on Tomorrow'
    doc = nlp(test_text)
    print("Entities in '%s'" % test_text)
    for ent in doc.ents:
        print(ent.label_, ent.text)

    # save model to output directory
    print('output_dir')
    output_dir ='models/travel_date'
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.meta['name'] = new_model_name  # rename model
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        for ent in doc2.ents:
            print(ent.label_, ent.text)

if __name__ == '__main__':
    plac.call(train_entity)

