from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'entity_recognition'
