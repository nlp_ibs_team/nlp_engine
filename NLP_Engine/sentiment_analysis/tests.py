data={
    "Entity_recognition": {
        "Dates": [
            "on 14th April 2019 at 4:30 in the morning",
            "sun"
        ],
        "From_Dates": [
            "14th April 2019"
        ],
        "Times": [
            "4:30 in the morning"
        ],
        "Source Airport": [
            "COK"
        ],
        "Destination Airport": [
            "BLR"
        ],
        "Airport": [
            "PNR"
        ]
    },
    "Sentiment_analysis": [
        {
            "sentence": "Sentence 1",
            "value": 0.6000000000000001
        },
        {
            "sentence": "Sentence 2",
            "value": 0
        },
        {
            "sentence": "Sentence 3",
            "value": 0
        },
        {
            "sentence": "Sentence 4",
            "value": -0.125
        }
    ],
    "Intent Classification": {
        "Response": [
            "Retro Claim",
            1
        ]
    }
}
print(data['Intent Classification']['Response'])