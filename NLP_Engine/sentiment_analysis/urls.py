from django.conf.urls import url
from .views import Sentiment_Analysis
urlpatterns = [

    url(r'^sentiment_analysis/', Sentiment_Analysis, name="sentiment_analysis"),
]