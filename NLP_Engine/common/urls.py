from django.conf.urls import url
from .views import CommonAPI
urlpatterns = [

    url(r'^common_api/', CommonAPI, name="common_api"),
]