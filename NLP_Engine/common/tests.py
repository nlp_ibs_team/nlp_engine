import asyncio
import time
from datetime import datetime

parse_response = dict()
async def custom_sleep():
    print('SLEEP', datetime.now())
    time.sleep(1)
async def sentiment(name):

    print('Exicute sentiment',name)
    parse_response.update({'a':name})
    return parse_response

async def indent_clasification(name):
    print('Exicute Indent_clasification', name)
    parse_response.update({'b':name})
    return parse_response


async def Entity_reco(name):
        print('Exicute Entity_reco', name)
        parse_response.update({'c':name})
        return parse_response


start = time.time()
loop = asyncio.get_event_loop()
tasks = [
    asyncio.ensure_future(sentiment("A")),
    asyncio.ensure_future(indent_clasification("B")),
    asyncio.ensure_future(Entity_reco("C"))
]

a= loop.run_until_complete(asyncio.wait(tasks))
for fut in a:

    print(fut)

loop.close()
